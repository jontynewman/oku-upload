<?php

namespace JontyNewman\Oku\Upload;

use JontyNewman\Oku\Context\InputInterface;
use JontyNewman\Oku\Helpers\Html;
use JontyNewman\Oku\Upload\Repository;

/**
 * A form input for uploading a single file.
 */
class Input implements InputInterface
{
	/**
	 * The name of the input.
	 *
	 * @var string
	 */
	private $name;

	/**
	 * Constructs a form input for uploading a single file.
	 *
	 * @param string|null $name The name of the input (or NULL to use the
	 * default).
	 */
	public function __construct(string $name = null)
	{
		$this->name = $name ?? Repository::NAME;
	}

	public function name(): string
	{
		return $this->name;
	}

	public function value(): string
	{
		return '';
	}

	public function html(
			array $attributes = [],
			int $flags = null,
			string $encoding = null
	): string {

		$immutable = ['name' => $this->name];
		return Html::tag('input', $immutable + $attributes, $flags, $encoding);
	}

	public function __toString(): string
	{
		return $this->html(['type' => 'file']);
	}
}
