<?php

namespace JontyNewman\Oku\Upload;

use ArrayAccess;
use DirectoryIterator;
use EmptyIterator;
use InvalidArgumentException;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\File;
use JontyNewman\Oku\ResponseBuilderInterface;
use JontyNewman\Oku\Upload\Input;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use RuntimeException;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converters\ShiftConverter;
use Symfony\Component\Filesystem\Filesystem;
use UnexpectedValueException;

/**
 * A repository for persisting uploaded files.
 */
class Repository implements ArrayAccess
{
	/**
	 * The default name for the uploaded file part of the request body.
	 */
	const NAME = 'upload';

	/**
	 * The default mode of directories created by the repository.
	 */
	const MODE = 0770;

	/**
	 * The prefix currently being used to persist files.
	 *
	 * @var string
	 */
	private $prefix;

	/**
	 * The name currently being associated with the uploaded file part of the
	 * request body.
	 *
	 * @var string
	 */
	private $name;

	/**
	 * The mode currently being used when creating directories.
	 *
	 * @var int
	 */
	private $mode;

	/**
	 * The size of the buffer being used when files are rendered (if any).
	 *
	 * @var int|null
	 */
	private $buffer;

	/**
	 * The callback being used to determine the content type of uploads.
	 *
	 * @var callable|null
	 */
	private $type;

	/**
	 * The last directory that was proven to exist (if any).
	 *
	 * @var string|null
	 */
	private $directory;

	/**
	 * The last file that was proven to exist (if any).
	 *
	 * @var string|null
	 */
	private $file;

	/**
	 * Constructs a repository for persisting uploaded files.
	 *
	 * The type callback will be passed the path of the file as the first
	 * argument.
	 *
	 * @param string $prefix The prefix to use when persisting file (e.g. an
	 * absolute path to the directory containing uploads with a trailing
	 * directory separator).
	 * @param string|null $name The name to associate with the uploaded file
	 * part of the request body (or NULL to use the default).
	 * @param int|null $mode The mode to use when creating directories (or NULL
	 * to use the default).
	 * @param int|null $buffer The size of the buffer to use when rendering
	 * files (or NULL to use the default).
	 * @param callable|null $type The callback to use in order to determine the
	 * content type of a given upload (or NULL to use the default
	 * approximation).
	 */
	public function __construct(
		string $prefix,
		string $name = null,
		int $mode = null,
		int $buffer = null,
		callable $type = null
	) {
		$this->prefix = $prefix;
		$this->name = $name ?? self::NAME;
		$this->mode = $mode ?? self::MODE;
		$this->buffer = $buffer;
		$this->type = $type;
	}

	/**
	 * Generates a form input for uploading files.
	 *
	 * @return \JontyNewman\Oku\Upload\Input The generated input.
	 */
	public function input(): Input
	{
		return new Input($this->name);
	}

	public function offsetExists($offset): bool
	{
		return !is_null($this->file($this->encode((string) $offset)));
	}

	public function offsetGet($offset)
	{
		return function (
			ResponseBuilderInterface $builder,
			ContextInterface $context
		) use ($offset) {

			$path = $this->file($this->encode((string) $offset));

			if (is_null($path)) {
				throw new UnexpectedValueException('Cannot convert offset to an existing file');
			}

			$headers = $context->request()->getHeaders();
			$shifted = (new ShiftConverter())->convert($headers);
			$type = $this->toType($path);

			if (!File::build($builder, $path, $shifted, $this->buffer, $type)) {
				throw new RuntimeException("Cannot build response from path '{$path}'");
			}
		};
	}

	public function offsetSet($offset, $value): void
	{
		if (!($value instanceof ServerRequestInterface)) {
			throw new InvalidArgumentException('Expected value to be a PSR-compliant server request');
		}

		$upload = $value->getUploadedFiles()[$this->name] ?? null;

		if (!($upload instanceof UploadedFileInterface)) {
			throw new UnexpectedValueException('Cannot retrieve uploaded file from request');
		}

		$error = $upload->getError();

		if (UPLOAD_ERR_OK !== $error) {
			throw new UnexpectedValueException('An error is associated with the uploaded file', $error);
		}

		$this->offsetUnset($offset);

		$path = $this->encode((string) $offset);

		if (!mkdir($path, $this->mode, true)) {
			throw new RuntimeException("Cannot create directory '{$path}'");
		}

		$filename = basename($upload->getClientFilename() ?? $this->name);

		if (in_array($filename, ['', '.', '..'])) {
			throw new UnexpectedValueException("Filename '{$filename}' is invalid");
		}

		$upload->moveTo("{$path}{$this->separator()}{$filename}");
	}

	public function offsetUnset($offset): void
	{
		$path = $this->encode((string) $offset);

		(new Filesystem())->remove($path);

		// If the existence of the given offset is being cached, expire it.
		if ($path === $this->directory) {
			$this->directory = null;
			$this->file = null;
		}
	}

	/**
	 * Converts the given path to the absolute path of the associated directory.
	 *
	 * @param string $path The path to convert.
	 * @return string The converted path.
	 */
	private function encode(string $path): string
	{
		if ('' === $path) {
			$path = '/';
		}

		$encoded = File::path($path);
		return "{$this->prefix}{$encoded}";
	}

	/**
	 * Converts the given path to the absolute path of the associated file (if
	 * any).
	 *
	 * @param string $path The path to convert.
	 * @return string|null The converted path.
	 */
	private function file(string $path): ?string
	{
		$file = null;
		$first = null;

		if ($path === $this->directory) {
			$file = $this->file;
		} else {
			$first = $this->first($path);
		}

		if (!is_null($first)) {
			$this->directory = $path;
			$this->file = $first;
			$file = $first;
		}

		return $file;
	}

	/**
	 * Converts the given directory path to the absolute path of an immediate
	 * child file (if any).
	 *
	 * @param string $path The path to convert.
	 * @return string|null The converted path.
	 */
	private function first(string $path): ?string
	{
		$file = null;
		$directory = file_exists($path)
			? new DirectoryIterator($path)
			: new EmptyIterator();

		foreach ($directory as $child) {

			if ($child->isFile()) {
				$file = $child->getPathname();
				break;
			}
		}

		return $file;
	}

	/**
	 * Determines the current directory separator.
	 *
	 * @return string The current directory separator.
	 */
	private function separator(): string
	{
		return (string) DIRECTORY_SEPARATOR;
	}

	/**
	 * Converts the given file path to a content type using the current callback
	 * (if any).
	 *
	 * @param string $path The path of the file to convert.
	 * @return string|null The converted file path.
	 * @throws UnexpectedValueException The callback returned a value that is
	 * neither a string nor NULL.
	 */
	private function toType(string $path): ?string
	{
		$type = null;

		if (!is_null($this->type)) {
			$type = ($this->type)($path);
		}

		if (!is_string($type ?? '')) {
			throw new UnexpectedValueException('Expected return value of type callback to be string or NULL');
		}

		return $type;
	}
}
