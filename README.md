# JontyNewman\Oku\Upload

Allows files to be uploaded and served by
[JontyNewman\Oku](https://gitlab.com/jontynewman/oku).

## Installation

```
composer require 'jontynewman/oku-upload ^1.0'
```

## Example Usage

```php
<?php

use GuzzleHttp\Psr7\Response;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\RequestHandler;
use JontyNewman\Oku\ResponseBuilderInterface;
use JontyNewman\Oku\Upload\Repository;
use JontyNewman\Oku\Upload\Input;

require 'vendor/autoload.php';

$repository = new Repository('/path/to/uploads/');

$default = new Response(404, ['Content-Type' => 'text/plain'], 'Not Found');

$editor = function (
    ResponseBuilderInterface $builder,
    ContextInterface $context
) use ($repository): void {

    $builder->content(function () use ($context, $repository) {
        require 'editor.php';
    });
};

$handler = new RequestHandler($repository, $default, $editor);

$handler->run();

```

```php
<?php

// editor.php

/* @var $context \JontyNewman\Oku\ContextInterface */
/* @var $repository \JontyNewman\Oku\Upload\Repository */

?>
<!DOCTYPE html>
<html>
  <head>
    <title>JontyNewman\Oku\Upload</title>
  </head>
  <body>
    <form action="" method="post" enctype="multipart/form-data">
      <p>
        <label>
          File
          <?= $repository->input(); ?>
        </label>
      </p>
      <p>
        <?= $context->token(); ?>
        <?= $context->put(); ?>
        <input type="submit" value="Upload">
      </p>
    </form>
    <form action="" method="post">
      <p>
        <?= $context->token(); ?>
        <?= $context->delete(); ?>
        <input type="submit" value="Delete">
      </p>
    </form>
    <?= $context->inset(); ?>
  </body>
</html>

```
