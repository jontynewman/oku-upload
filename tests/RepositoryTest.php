<?php

namespace JontyNewman\Oku\Upload\Tests;

use ArrayObject;
use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\UploadedFile;
use InvalidArgumentException;
use JontyNewman\Oku\Context\SessionInterface;
use JontyNewman\Oku\Contexts\RequestHandlerContext;
use JontyNewman\Oku\File;
use JontyNewman\Oku\RequestHandler;
use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use JontyNewman\Oku\Upload\Repository;
use JontyNewman\Oku\Upload\Tests\InputTest;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use ShrooPHP\Framework\Application;
use SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use UnexpectedValueException;

class RepositoryTest extends TestCase
{
	public function testRepository()
	{
		$this->assertRepository();
	}

	public function testRepositoryWithNameAndModeAndType()
	{
		$this->assertRepository('name', 0700, 'text/css');
	}

	public function testRepositoryWithInvalidTypeReturnValue()
	{
		$exception = null;
		$path = '/';
		$separator = DIRECTORY_SEPARATOR;
		$directory = __FUNCTION__;
		$root = vfsStream::setup();
		$prefix = "{$root->url()}{$separator}{$directory}{$separator}";
		$type = function () {
			return (object) null;
		};
		$request = new ServerRequest('POST', $path);
		$upload = $this->toUploadedFile("{$root->url()}{$separator}_upload");
		$uploads = [Repository::NAME => $upload];
		$repository = new Repository($prefix, null, null, null, $type);
		$builder = new RequestHandlerResponseBuilder();
		$context = $this->toContext($request->getMethod(), $path);

		$repository->offsetSet($path, $request->withUploadedFiles($uploads));

		try {
			($repository->offsetGet($path))($builder, $context);
		} catch (UnexpectedValueException $exception) {
			$this->assertSame(
					'Expected return value of type callback to be string or NULL',
					$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
	}

	public function assertRepository(
			string $name = null,
			int $mode = null,
			string $type = null
	) {
		$exception = null;
		$path = '/';
		$separator = DIRECTORY_SEPARATOR;

		$root = vfsStream::setup();
		$prefix = "{$root->url()}{$separator}uploads{$separator}";
		$builder = new RequestHandlerResponseBuilder();
		$request = new ServerRequest('POST', $path);
		$expected = new class() {

			public $string = '';

			public function __toString(): string
			{
				return $this->string;
			}
		};
		$callback = is_null($type) ? null : $this->toCallback($expected, $type);
		$repository = new Repository($prefix, $name, $mode, null, $callback);

		InputTest::assert($this, $repository->input(), $name);

		if (is_null($name)) {
			$name = Repository::NAME;
		}

		if (is_null($mode)) {
			$mode = Repository::MODE;
		}

		$this->assertFalse($repository->offsetExists($path));

		$callback = $repository->offsetGet($path);

		$this->assertTrue(is_callable($callback));

		try {
			($callback)($builder, $this->toContext('GET', $path));
		} catch (UnexpectedValueException $exception) {
			$this->assertEquals(
				'Cannot convert offset to an existing file',
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		try {
			$repository->offsetSet($path, null);
		} catch (InvalidArgumentException $exception) {
			$this->assertEquals(
				'Expected value to be a PSR-compliant server request',
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		try {
			$repository->offsetSet($path, $request);
		} catch (UnexpectedValueException $exception) {
			$this->assertEquals(
				'Cannot retrieve uploaded file from request',
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		$this->assertTrue(mkdir($prefix, 0, true));

		$filepath = "{$root->url()}{$separator}_upload";

		$upload = $this->toUploadedFile($filepath);
		$request = $request->withUploadedFiles([$name => $upload]);

		try {
			$repository->offsetSet($path, $request);
		} catch (RuntimeException $exception) {
			$encoded = File::path($path, null, $separator);
			$this->assertEquals(
				"Cannot create directory '{$prefix}{$encoded}'",
				$exception->getMessage()
			);
		}

		$upload = $this->toUploadedFile($filepath, UPLOAD_ERR_EXTENSION);
		$request = $request->withUploadedFiles([$name => $upload]);

		try {
			$repository->offsetSet($path, $request);
		} catch (RuntimeException $exception) {
			$this->assertEquals(
				'An error is associated with the uploaded file',
				$exception->getMessage()
			);
			$this->assertEquals(UPLOAD_ERR_EXTENSION, $exception->getCode());
		}

		$this->assertNotNull($exception);
		$exception = null;

		$this->assertTrue(chmod($prefix, 0700));

		foreach (['', '.', '..'] as $filename) {

			$upload = $this->toUploadedFile($filepath, null, $filename);
			$request = $request->withUploadedFiles([$name => $upload]);

			try {
				$repository->offsetSet($path, $request);
			} catch (UnexpectedValueException $exception) {
				$this->assertEquals(
					"Filename '{$filename}' is invalid",
					$exception->getMessage()
				);
			}

			$this->assertNotNull($exception);
			$exception = null;
		}

		$upload = $this->toUploadedFile($filepath);
		$request = $request->withUploadedFiles([$name => $upload]);

		$repository->offsetSet($path, $request);

		$this->assertTrue($repository->offsetExists(''));
		$this->assertTrue($repository->offsetExists($path));

		$encoded = File::path($path, null, $separator);
		(new Filesystem())->remove("{$prefix}{$encoded}");

		$expected->string = "{$prefix}{$encoded}{$separator}{$name}";
		$callback = $repository->offsetGet($path);

		$this->assertTrue(is_callable($callback));

		try {
			($callback)($builder, $this->toContext('GET', $path));
		} catch (RuntimeException $exception) {
			$this->assertEquals(
				"Cannot build response from path '{$expected->string}'",
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		$upload = $this->toUploadedFile($filepath);
		$request = $request->withUploadedFiles([$name => $upload]);
		$repository->offsetSet($path, $request);

		$info = new SplFileInfo("{$prefix}{$encoded}");
		$this->assertEquals($mode + 0x4000, $info->getPerms());

		$this->assertTrue($repository->offsetExists(''));
		$this->assertTrue($repository->offsetExists($path));

		$callback = $repository->offsetGet($path);

		$this->assertTrue(is_callable($callback));

		($callback)($builder, $this->toContext('GET', $path));
		$response = $builder->response();

		$this->assertNull($response->code());

		$headers = iterator_to_array($response->headers());
		$this->assertNotEmpty($headers);

		if (!is_null($type)) {
			$this->assertSame($type, $headers['Content-Type'] ?? null);
		}

		ob_start();
		$response->content()->run();
		$this->assertEquals(file_get_contents(__FILE__), ob_get_clean());

		$repository->offsetUnset($path);

		$this->assertFalse($repository->offsetExists(''));
		$this->assertFalse($repository->offsetExists($path));
	}

	private function toContext(
		string $method,
		string $path
	): RequestHandlerContext {

		$session = new class() extends ArrayObject implements SessionInterface {

			public function commit(): void
			{
				// Do nothing.
			}
		};

		return new RequestHandlerContext(
			new ServerRequest($method, $path),
			$session,
			RequestHandler::INSET,
			Application::METHOD,
			Application::TOKEN,
			''
		);
	}

	private function toUploadedFile(
		string $path,
		int $error = null,
		string $filename = null
	) {

		$this->assertTrue(copy(__FILE__, $path));

		if (is_null($error)) {
			$error = UPLOAD_ERR_OK;
		}

		return new UploadedFile($path, filesize($path), $error, $filename);
	}

	private function toCallback($expected, string $type): callable
	{
		return function (string $path) use ($expected, $type) {

			$this->assertSame((string) $expected, $path);

			return $type;
		};
	}
}
