<?php

namespace JontyNewman\Oku\Upload\Tests;

use JontyNewman\Oku\Helpers\Html;
use JontyNewman\Oku\Upload\Input;
use JontyNewman\Oku\Upload\Repository;
use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{
	public static function assert(
		TestCase $test,
		Input $input,
		string $name = null
	) {
		$tag = 'input';
		$value = '<"value">';
		$charset = 'ISO-8859-1';
		$flags = ENT_NOQUOTES | ENT_HTML5;
		$text = ['type' => 'text', 'value' => $value];
		$file = ['type' => 'file'];

		if (is_null($name)) {
			$name = Repository::NAME;
		}

		$test->assertEquals($name, $input->name());
		$test->assertSame('', $input->value());

		$attrs = ['name' => $name];
		$test->assertEquals(Html::tag($tag, $attrs), $input->html());
		$test->assertEquals(Html::tag($tag, $attrs + $file), (string) $input);

		$test->assertEquals(
			Html::tag($tag, $attrs + $text),
			$input->html($text)
		);

		$test->assertEquals(
			Html::tag($tag, $attrs + $text, $flags, $charset),
			$input->html($text, $flags, $charset)
		);
	}

	public function testInput()
	{
		self::assert($this, new Input());
	}

	public function testInputWithName()
	{
		$name = '<"name">';
		self::assert($this, new Input($name), $name);
	}
}
